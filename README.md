# Agora

WebRTC project to communicate in a constructive, inclusive and respectful way.

## Comunication, the GA way

Communicate better with techniques learned mainly during general assemblies.

### Only one persone speaks

And nobody will cut them off until they're done. Listeners can queue up to speak next.

### Give your feedback without disturbing anyone

Send simple feedback like *agree*, *disagree*, *elaborate* or *more concise* that will display to everyone without drawing too much attention.

## Low-tech stack

All technologies have been chosen to be low power consuming.
- Svelte (https://svelte.dev/)
- Janus (https://github.com/meetecho/janus-gateway)
- Rust with tokio-tungstenite (https://github.com/snapview/tokio-tungstenite)

Janus is the WebRTC server that receives audio and video streams from the speaker and forwards them to all listeners. A Javascript lib is also used in the frontend to communicate with the Janus server.

The Rust backend coordinates clients with Websockets, it sends messages to clients informing who speaks and what feedbacks are given.

## Installation

**TODO**

There is no easy way ton deploy the app yet.
