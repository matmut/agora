use core::fmt;
use std::{
    collections::{HashMap, VecDeque},
    env,
    io::Error,
    net::SocketAddr,
    sync::{Arc, Mutex},
};

use futures_util::{stream::SplitSink, SinkExt, StreamExt};
use log::{debug, error, info, warn};
use tokio::net::{TcpListener, TcpStream};
use tokio_tungstenite::{tungstenite::Message, WebSocketStream};

#[derive(serde::Deserialize, serde::Serialize, Debug)]
#[serde(untagged)]
enum PacketBody {
    Str(String),
    List(Vec<String>),
}

#[derive(serde::Deserialize, serde::Serialize, Debug)]
// This is what will transit between clients and the server
struct Packet {
    #[serde(default)]
    category: String,
    message: PacketBody,
}

type PeersMap =
    Arc<Mutex<HashMap<SocketAddr, (SplitSink<WebSocketStream<TcpStream>, Message>, String)>>>;

#[derive(Clone)]
struct AppState {
    speaker_queue: Arc<Mutex<VecDeque<SocketAddr>>>,
    connected_peers: PeersMap,
}

// Display the AppState nicely
impl fmt::Display for AppState {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        writeln!(f, "Speaker queue: {:?}", self.speaker_queue.lock().unwrap())?;
        let display_map: HashMap<SocketAddr, String> = self
            .connected_peers
            .lock()
            .unwrap()
            .iter()
            .map(|(key, (_, name))| (key.to_owned(), name.to_owned()))
            .collect();
        writeln!(f, "Connected peers: {:?}", display_map)?;
        return Ok(());
    }
}

fn get_packet(value: Result<Message, tokio_tungstenite::tungstenite::Error>) -> Option<Packet> {
    // Verbose way of handling all errors that can occur from the WebSocketStream's message and the parsed Packed
    match value {
        Err(error) => {
            warn!("Error reading WebSocket Stream: {:?}", error);
            return None;
        }
        Ok(message) => match message.to_text() {
            Err(error) => {
                error!("Error getting text from Stream Message: {:?}", error);
                return None;
            }
            Ok(text_message) => {
                // Attempt parsing text to JSON
                let serde_res: Result<Packet, serde_json::Error> =
                    serde_json::from_str(text_message);
                match serde_res {
                    Err(error) => {
                        error!("Error deserializing text: {:?}", error);
                        return None;
                    }
                    Ok(packet) => {
                        debug!("Got packet: {:?}", packet);
                        return Some(packet);
                    }
                }
            }
        },
    }
}

fn let_peer_speak(peer_addr: SocketAddr, app_state: &AppState) {
    let packet = Packet {
        category: "new_speaker".to_string(),
        message: PacketBody::Str("you_can_speak".to_string()),
    };
    send_packet_to_addr(&peer_addr, app_state, &packet)
}

fn add_peer_to_speaker_queue(peer_addr: SocketAddr, app_state: &AppState) {
    debug!("Want to add {:?} to speaker queue", peer_addr);

    // Lock from here to test if peer already and queue AND insert them or not
    let mut queue_guard = app_state.speaker_queue.lock().unwrap();

    // Peer cannot be in the queue multiple times
    if !queue_guard.contains(&peer_addr) {
        queue_guard.push_back(peer_addr);
        info!("Added {:?} to speaker queue", peer_addr);
    } else {
        warn!("Peer was already in the queue");
    }
}

fn log_app_state(app_state: &AppState) {
    debug!("Current app state:\n{}", app_state);
}

fn broadcast_speaker_queue(app_state: &AppState) {
    debug!("Broadcasting speaker queue");

    // Constructing speaker queue with name instead of IPs
    let mut queue_with_names: Vec<String> = Vec::new();
    for peer_addr in app_state.speaker_queue.lock().unwrap().iter() {
        let connected_peers = app_state.connected_peers.lock().unwrap();

        // Drop the Sink and push the name string
        let (_, name) = connected_peers.get(peer_addr).unwrap().to_owned();
        queue_with_names.push(name.to_owned());
    }

    // Clients expect the speaker queue with category == speaker_queue
    let packet = Packet {
        category: "speaker_queue".to_string(),
        message: PacketBody::List(queue_with_names),
    };
    broadcast_packet(app_state, &packet)
}

fn broadcast_peers_list(app_state: &AppState) {
    debug!("Broadcasting peers list");

    // Constructing peers list with usernames
    // TODO use a map ?
    let mut usernames_list: Vec<String> = Vec::new();
    for (_, username) in app_state.connected_peers.lock().unwrap().values() {
        usernames_list.push(username.to_owned());
    }

    // Clients expect the participants list with category == participants_list
    let packet = Packet {
        category: "participants_list".to_string(),
        message: PacketBody::List(usernames_list),
    };
    broadcast_packet(app_state, &packet);
}

fn broadcast_packet(app_state: &AppState, packet: &Packet) {
    // Send to all connected peers
    for (peer_addr, (sink, _)) in app_state.connected_peers.lock().unwrap().iter_mut() {
        send_packet_to_sink(&peer_addr, sink, packet)
    }
}

// Send a message to a given Sink
// When you call this, you typically locked the app_state.connected_peers yourself
fn send_packet_to_sink(
    peer_addr: &SocketAddr,
    sink: &mut SplitSink<WebSocketStream<TcpStream>, Message>,
    packet: &Packet,
) {
    debug!("Sending packet to {:?} :", peer_addr);
    debug!("{:?}", packet);

    // SinkExt::send is async
    // We block so we don't have to make broadcast_message async
    // TODO maybe this should be async
    futures::executor::block_on(sink.send(Message::text(serde_json::to_string(packet).unwrap())))
        .unwrap();
}

// Send a message to a SocketAddr
// This function needs to lock the app_state.connected_peers itself to get the corresponding sink
fn send_packet_to_addr(peer_addr: &SocketAddr, app_state: &AppState, packet: &Packet) {
    debug!("Sending packet to {:?} :", peer_addr);
    debug!("{:?}", packet);

    let mut guard = app_state.connected_peers.lock().unwrap();
    let (sink, _) = guard.get_mut(&peer_addr).unwrap();
    // SinkExt::send is async
    // We block so we don't have to make broadcast_message async
    // TODO maybe this should be async
    futures::executor::block_on(sink.send(Message::text(serde_json::to_string(packet).unwrap())))
        .unwrap();
}

fn close_connection(app_state: &AppState, peer_addr: SocketAddr) {
    info!("Closing connection with {:?}", peer_addr);
    app_state.connected_peers.lock().unwrap().remove(&peer_addr);
    app_state
        .speaker_queue
        .lock()
        .unwrap()
        // This removes the peer equals to peer_addr
        .retain(|peer| peer != &peer_addr);
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    // Need to init env_logger but don't need the result
    env_logger::init_from_env(
        env_logger::Env::default().filter_or(env_logger::DEFAULT_FILTER_ENV, "debug"),
    );

    // Choose an IP address for the server
    let addr = env::args()
        .nth(1)
        .unwrap_or_else(|| "127.0.0.1:55555".to_string());

    // Init AppState, no peers and no one in queue
    let app_state: AppState = AppState {
        connected_peers: Arc::new(Mutex::new(HashMap::new())),
        speaker_queue: Arc::new(Mutex::new(VecDeque::new())),
    };
    log_app_state(&app_state);

    // Create the event loop and TCP listener we'll accept connections on.
    let try_socket = TcpListener::bind(&addr).await;
    let listener = try_socket.expect("Failed to bind");
    info!("Listening on: {}", addr);

    // Accept incoming new connections
    while let Ok((stream, _)) = listener.accept().await {
        // Spawn a tokio task (asynchronous) and give it the new peer's TcpStream
        tokio::spawn(accept_connection(app_state.clone(), stream));
    }

    Ok(())
}

async fn accept_connection(app_state: AppState, stream: TcpStream) {
    // Get peer's IP address from TcpStream object
    let peer_addr = stream
        .peer_addr()
        .expect("Connected streams should have a peer address");

    // Init WebSocketStream from TcpStream
    let ws_stream = tokio_tungstenite::accept_async(stream)
        .await
        .expect("Error during the websocket handshake occurred");

    info!("New WebSocket connection with {}", peer_addr);

    // Separate WebSocketStream's write and read parts
    // We only need to store the write part
    // The read part will only be used here, in the tokio task
    let (write, mut read) = ws_stream.split();

    // Store the write part
    app_state
        .connected_peers
        .lock()
        .unwrap()
        .insert(peer_addr, (write, String::new()));

    // Here we expect the client to send us its name in a hello packet
    // Otherwise connection is not correctly initialized and we close it
    if let Some(value) = read.next().await {
        if let Some(packet) = get_packet(value) {
            match packet {
                Packet {
                    category,
                    message: PacketBody::Str(message),
                } if category == "hello" => {
                    let mut guard = app_state.connected_peers.lock().unwrap();
                    let (sink, _) = guard.remove(&peer_addr).unwrap();
                    guard.insert(peer_addr, (sink, message));
                }
                _ => {
                    error!("Client did not initialize connection properly, needs to send its name");
                    close_connection(&app_state, peer_addr);
                }
            }
        } else {
            error!("Client did not initialize connection properly, needs to send its name");
            close_connection(&app_state, peer_addr);
        }
    } else {
        error!("Client did not initialize connection properly, needs to send its name");
        close_connection(&app_state, peer_addr);
    }

    log_app_state(&app_state);

    // Give speaker queue state to the new client
    // TODO : maybe don't broadcast it here
    broadcast_speaker_queue(&app_state);

    // Inform peers that a new peer is connected
    broadcast_peers_list(&app_state);

    // Read Message from the WebSocketStream's write part until we get a None
    while let Some(value) = read.next().await {
        // Parse Message and go on if successfully parsed a Packet
        if let Some(packet) = get_packet(value) {
            if let PacketBody::Str(message_str) = packet.message {
                match (packet.category.as_str(), message_str.as_str()) {
                    (_, "want_to_speak") => {
                        info!("Peer {:?} wants to get in the queue", peer_addr);
                        // Add peer to the queue
                        add_peer_to_speaker_queue(peer_addr, &app_state);
                        // If peer is alone in queue, he becomes the speaker
                        if app_state.speaker_queue.lock().unwrap().len() == 1 {
                            let_peer_speak(peer_addr, &app_state)
                        }
                        broadcast_speaker_queue(&app_state);
                        log_app_state(&app_state);
                    }
                    (_, "done_speaking") => {
                        let mut guard = app_state.speaker_queue.lock().unwrap();
                        guard.pop_front();
                        let peer_res = guard.front().copied();
                        drop(guard);
                        match peer_res {
                            Some(peer_addr) => {
                                let_peer_speak(peer_addr, &app_state);
                            }
                            None => {}
                        }
                        broadcast_speaker_queue(&app_state);
                    }
                    ("feedback", message) => {
                        info!("Received feedback : {:?}", message);
                        let message_to_send = format!("{} says : {}", app_state.connected_peers.lock().unwrap().get(&peer_addr).unwrap().1, message);
                        let packet = Packet {
                            category: String::from("feedback"),
                            message: PacketBody::Str(message_to_send),
                        };
                        broadcast_packet(&app_state, &packet);
                    }
                    ("ping", _) => {
                        info!("Received ping from : {}", peer_addr);
                        let packet = Packet {
                            category: String::from("pong"),
                            message: PacketBody::Str("".to_owned())
                        };
                        send_packet_to_addr(&peer_addr, &app_state, &packet);
                    }
                    _ => {
                        error!("Received unknown message from {:?}", peer_addr)
                    }
                }
            }
        }
    }

    // Loop is over means we got a None instead of a Message
    close_connection(&app_state, peer_addr);
    // Inform peers that a peer has disconnected
    broadcast_peers_list(&app_state);
    log_app_state(&app_state);
}
